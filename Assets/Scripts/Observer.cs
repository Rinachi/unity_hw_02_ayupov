﻿using UnityEngine;

/// <summary>
/// Камера слежения за объектом.
/// </summary>
public class Observer : MonoBehaviour
{
    [SerializeField] private GameObject observable;     // в переводе "наблюдаемый"

    [Range(1, 30)] public float height = 3;

    private void Start()
    {
        transform.localEulerAngles = new Vector3(90, 0, 0);
        transform.position = observable.transform.position;
    }

    private void LateUpdate()
    {
        var objPos = observable.transform.position;
        transform.position = new Vector3(objPos.x, objPos.y + height, objPos.z);
    }
}
