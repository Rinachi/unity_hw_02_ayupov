﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] [Tooltip("Object \"G\"")] private Game currentGame;
    [SerializeField] private Text startMessage;
    [SerializeField] private Text time;
    [SerializeField] private UIPopup popupMessage;
    [SerializeField] private Text message;

    private void Start()
    {
        if (startMessage.gameObject.activeSelf)
        {
            StartCoroutine(FadingTextCoroutine(startMessage, 0.02f, 3));
        }
        popupMessage.Close();
    }
    private void Update()
    {
        time.text = currentGame.left_time_in_display.ToString();
        if (!Game.IsGame())
        {
            if (!popupMessage.gameObject.activeSelf)
            {
                if (Game.VictoryOrDefeat())
                    MessageParams(message, "You Win!!!", Color.green);
                else
                    MessageParams(message, "You Lose...", Color.red);
                popupMessage.Open();
            }
        }
    }

    private void MessageParams(Text text, string message, Color color)
    {
        text.text = message;
        text.color = color;
    }

    private IEnumerator FadingTextCoroutine(Text text, float step, float wait)
    {
        text.color = currentGame.first_marker_color_in_display;
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
        yield return new WaitForSecondsRealtime(1);
        float progress = 0;
        MessageParams(text, text.text, currentGame.first_marker_color_in_display);
        while (text.color.a < 1)
        {
            float alpha = Mathf.Lerp(0, 1, progress);
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
            progress += step;
            yield return null;
        }
        progress = 0;
        yield return new WaitForSecondsRealtime(wait);
        while (text.color.a > 0)
        {
            float alpha = 1 - Mathf.Lerp(0, 1, progress);
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
            progress += step;
            yield return null;
        }
        startMessage.gameObject.SetActive(false);
    }

    
}
