using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Cube))]
public class CubeInput : MonoBehaviour
{
    public bool TouchBegan
    {
        get { return Input.GetMouseButtonDown(0); }
    }

    public float horizontal;
    public float vertical;

    Vector3 accept_vector;

    Vector2 touch_dist;
    Vector2 start_touch_position;
    bool pressed;
    bool init_touch;

    public float max_distance = 31.0f;
    public RawImage base_joystick;
    public RawImage stick_joystick;

    public void Tick()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical =  Input.GetAxisRaw("Vertical");
        UpdateTouchInput();
        if (horizontal > vertical)
        {
            vertical = 0;
        }
        else
        {
            horizontal = 0;
        }
        accept_vector = new Vector3(horizontal, 0, vertical);
    }

    void UpdateTouchInput()
    {
        pressed = Input.GetMouseButton(0);

        if(base_joystick != null && stick_joystick != null)
        {
            base_joystick.gameObject.SetActive(pressed);
            stick_joystick.gameObject.SetActive(pressed);
        }

        if (pressed)
        {
            if(!init_touch)
            {
                start_touch_position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                init_touch = true;
                if(base_joystick != null)
                    base_joystick.transform.position = new Vector3(start_touch_position.x, start_touch_position.y, 0.0f);
            }

            touch_dist = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - start_touch_position;

            var distance = Mathf.Min(touch_dist.magnitude, max_distance);
            touch_dist = touch_dist.normalized * distance;
            touch_dist.x = touch_dist.x / max_distance;
            touch_dist.y = touch_dist.y / max_distance;

            horizontal = touch_dist.x;
            vertical = touch_dist.y;

            if(stick_joystick != null)
                stick_joystick.transform.localPosition = new Vector3(horizontal * max_distance, vertical * max_distance, 0.0f);
        }
        else
        {
            touch_dist = Vector2.zero;
            init_touch = false;
        }
    }

    public Vector3 GetVector3()
    {
        return accept_vector;
    }
}
